#include <stdio.h>
#include <stdlib.h>

short *setInput(){
	short *n = malloc(sizeof(short));
	int i = 0, x;
	while(x = getchar(), x != '\n'){
		n[i] = x - 48;
		i++;
		n = realloc(n, sizeof(short) * (i + 1));
	}
	n[i] = -1;
	return n;
}

short *set(int x){
	int i, j;
	short t = x;
	for(i = 0; x /= 10; i++);
	short *n = malloc(sizeof(short) + (i + 2));
	for(j = i; j >= 0; j--){
		n[j] = t % 10;
		t /= 10;
	}
	n[i + 1] = -1;
	return n;
}

short* setFile(FILE *f){
	short *n = malloc(sizeof(short));
	int i = 0, x;
	while(!feof(f)){
		x = fgetc(f) - 48;
		if(x != -49 && x != -16){
			n[i] = x;
			i++;
			n = realloc(n, sizeof(short) * (i + 1));
		}
	}
	return n;
}

void printVect(short *v){
	for(;*v != -1; v++) printf("%d", *v);
	printf("\n");
}

short *add(short *n1, short *n2){
	short *r, *vt, *t1, *t2;
	int i = 0, m = 0, t, i1 = 0, i2 = 0, max = 0;
	while(n1[i1] != -1) i1++;
	while(n2[i2] != -1) i2++;
	t1 = malloc(sizeof(short) * i1);
	t2 = malloc(sizeof(short) * i2);
	for(i = 0; i < i1; i++) t1[i] = n1[i];
	for(i = 0; i < i2; i++) t2[i] = n2[i];
	max = i1 > i2 ? i1 : i2;
	if(i1 > i2){
		vt = malloc(sizeof(short) * (max + 1));
		for(i = 0; i <= max; i++) vt[i] = (i < (i1 - i2)) ? 0 : n2[i - (i1 - i2)];
		t2 = realloc(t2, sizeof(short) * (max + 1));
		for(i = 0; i < max; i++) t2[i] = vt[i];
		free(vt);
	}else if(i2 > i1){
		vt = malloc(sizeof(short) * (max + 1));
		for(i = 0; i <= max; i++) vt[i] = (i < (i2 - i1)) ? 0 : n1[i - (i2 - i1)];
		t1 = realloc(t1, sizeof(short) * (max + 1));
		for(i = 0; i < max; i++) t1[i] = vt[i];
		free(vt);
	}
	r = malloc(sizeof(short) * (max + 1));
	for(i = 0; i < max; i++) r[i] = 0;
	r[max] = -1;
	i = max - 1;
	while(i >= 0){
		t = t1[i] + t2[i] + m;
		r[i] = t % 10;
		m = t / 10;
		i--;
	}
	if(m){
		r = realloc(r, sizeof(short) * (max + 2));
		for(i = max + 1; i > 0; i--) r[i] = r[i - 1];
		r[0] = m;
	}
	free(t1);
	free(t2);
	return r;
}

int main(){
	short *n1, *n2;
	n1 = setInput();
	n2 = setInput();
	printVect(add(n1, n2));
	return 0;
}
