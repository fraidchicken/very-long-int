# Very Long Int

'cause a number of 128 bit is *really* too small.

This code offers a way to store big number in C and do some calculation on them.

***ISSUE: TO MUCH MEMORY IS USED*** ~~I'm trying to solve it, but don't really know how~~

**WIP:**
 - Only for positive integers
 - Only for sum

**To Do:**
 - Fix some bug
 - Add negative number
 - Add float (decimal)
 - Add other operation.